package umlsequencediagrammodel.sdm;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class Sdm_V1_UnitTest {
/**
 * All the commented files are for older version.
 * @author Anusree Poduval
 * @author Suishmitaa Srianand  
 */
//  @Test
  /**
   * Input file has no class.
   * @throws Exception
   */
 /* public void UnitTest_T1() throws Exception {
    
    String FILE_PATH = "C:\\Users\\ROHITH\\Desktop\\Tests\\test1.java"; //test1.java is an empty java file
    
    Parser.parseFile(FILE_PATH);
    assertTrue(Parser.umlClassList.isEmpty()); //this will throw error since it is in version 1.0 and we have changed it now.
  }*/  
  
//  @Test
  /**
   * Input file has one class.
   * @throws Exception
   */
/*  public void UnitTest_T2() throws Exception {
    
    String FILE_PATH = "C:\\Users\\ROHITH\\Desktop\\Tests\\\\test2.java"; //test2.java contains one class file
    
    Parser.parseFile(FILE_PATH);
    assertEquals("Test2",Parser.umlClassList.get(0).getName());//this will throw error since it is in version 1.1 and we have changed it now.
  }*/

  @SuppressWarnings("serial")
  @Test
  /**
   * Input file has multiple classes.
   * @throws Exception
   */
  public void UnitTest_T3() throws Exception {
    
    String FILE_PATH = "C:\\Users\\ROHITH\\Desktop\\Tests\\test3.java"; //test3.java contains multiple classes
    ArrayList<String> expect3 = new ArrayList<String>()  {
      {
        add("my:Computer");
    	add("your:Laptop");
      }
    };
    
    ArrayList<String> actual3 = new ArrayList<String>();
    ArrayList<UMLSequenceDiagram> allSDM = Parser.parseFile(FILE_PATH);
    
    actual3.add(allSDM.get(0).getSdm().get(1).getFrom());
    actual3.add(allSDM.get(0).getSdm().get(2).getTo());
    assertEquals(expect3, actual3);
    
  }

  @Test
  /**
   * Input file has a class with one method.
   * @throws Exception
   */
  public void UnitTest_T4() throws Exception {
    
    String FILE_PATH = "C:\\Users\\ROHITH\\Desktop\\Tests\\test4.java"; //test4.java contains main method
     
    ArrayList<UMLSequenceDiagram> allSDM = Parser.parseFile(FILE_PATH);
    assertEquals("main", allSDM.get(0).getMethod().getName());
  }

  @Test
  /**
   * Input file has a class with one method.
   * @throws Exception
   */
  public void UnitTest_T5() throws Exception {
    
    String FILE_PATH = "C:\\Users\\ROHITH\\Desktop\\Tests\\test4.java"; //test4.java contains main method
     
    ArrayList<UMLSequenceDiagram> allSDM = Parser.parseFile(FILE_PATH);
    assertEquals("display();", allSDM.get(0).getSdm().get(1).getMessageName());
  }

//  @Test
  /**
   * Input file has a class with constructor.
   * @throws Exception
   *//*
  public void UnitTest_T5() throws Exception {
    
    String FILE_PATH = "C:\\Users\\ROHITH\\Desktop\\Tests\\test5.java"; //test5.java contains a constructor

    Parser.parseFile(FILE_PATH);
    assertEquals(true, Parser.umlClassList.get(0).getUMLMethods().get(0).isConstructor());
  }

  @SuppressWarnings("serial")
  @Test
  *//**
   * Input file has a class with multiple methods.
   *//*
  public void UnitTest_T6() throws Exception {
    String FILE_PATH = "C:\\Users\\ROHITH\\Desktop\\Tests\\test6.java"; //test6.java contains multiple methods including multple constructors.
    ArrayList<String> expect5 = new ArrayList<String>()  {
      {
        add("Computer");
        add("computer_method");
        add("main");
        add("Laptop");
        add("laptop_method");
      }
    };
    ArrayList<String> actual5 = new ArrayList<String>();

    Parser.parseFile(FILE_PATH);

    for(UMLClass umlClass : Parser.umlClassList)  {
      for (int i = 0; i < umlClass.getUMLMethods().size(); i++) {
        actual5.add(umlClass.getUMLMethods().get(i).getName());
      }
    }
    assertEquals(expect5, actual5);
  }
  
  @Test
  *//**
   * Method has variable declarations, assignment statements and object instantiation statements along with method calls.
   *//*
  public void UnitTest_T7() throws Exception  {
    String FILE_PATH = "C:\\Users\\ROHITH\\Desktop\\Tests\\test7.java"; //test7.java contains method calls.
	Parser.parseFile(FILE_PATH); 
    assertEquals("my.computer_method();",Parser.umlClassList.get(0).getUMLMethods().get(2).getBlock().getStatement(0).toString());
    assertEquals("your.laptop_method();",Parser.umlClassList.get(0).getUMLMethods().get(2).getBlock().getStatement(1).toString());
  }

  *//**
   * Input file has method with "For" loop.
   * @throws Exception
   *//*
  @Test
  public void UnitTest_T8() throws Exception {
	  String FILE_PATH = "C:\\Users\\ROHITH\\Desktop\\Tests\\test8.java"; //test8.java contains a for loop that calls methods.
	  Parser.parseFile(FILE_PATH); 
      assertEquals("my.computer_method();",Parser.umlClassList.get(0).getUMLMethods().get(2).getBlock().getStatement(0).toString());
      assertEquals("your.laptop_method();",Parser.umlClassList.get(0).getUMLMethods().get(2).getBlock().getStatement(1).toString());
  }

  *//**
   * Input file has method with "while" loop.
   * @throws Exception
   *//*
  @Test
  public void UnitTest_T9() throws Exception {
	  String FILE_PATH = "C:\\Users\\ROHITH\\Desktop\\Tests\\test9.java"; //test9.java contains a while loop that calls methods.
	  Parser.parseFile(FILE_PATH); 
      assertEquals("my.computer_method();",Parser.umlClassList.get(0).getUMLMethods().get(2).getBlock().getStatement(0).toString());
      assertEquals("your.laptop_method();",Parser.umlClassList.get(0).getUMLMethods().get(2).getBlock().getStatement(1).toString());
  }

  *//**
   * Input file has method with "Do While" loop.
   * @throws Exception
   *//*
  @Test
  public void UnitTest_T10() throws Exception {
	  String FILE_PATH = "C:\\Users\\ROHITH\\Desktop\\Tests\\test10.java"; //test10.java contains a do while loop that calls methods.
	  Parser.parseFile(FILE_PATH); 
      assertEquals("my.computer_method();",Parser.umlClassList.get(0).getUMLMethods().get(2).getBlock().getStatement(0).toString());
      assertEquals("your.laptop_method();",Parser.umlClassList.get(0).getUMLMethods().get(2).getBlock().getStatement(1).toString());
  }

  *//**
   * Input file has method with "For each" loop.
   * @throws Exception
   *//*
  @Test
  public void UnitTest_T11() throws Exception {
	  String FILE_PATH = "C:\\Users\\ROHITH\\Desktop\\Tests\\test11.java"; //test11.java contains a for each loop that calls methods.
	  Parser.parseFile(FILE_PATH); 
      assertEquals("my.computer_method();",Parser.umlClassList.get(0).getUMLMethods().get(2).getBlock().getStatement(0).toString());
      assertEquals("your.laptop_method();",Parser.umlClassList.get(0).getUMLMethods().get(2).getBlock().getStatement(1).toString());
  }*/
}


