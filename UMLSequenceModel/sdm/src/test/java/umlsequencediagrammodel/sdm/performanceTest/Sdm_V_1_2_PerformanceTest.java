package umlsequencediagrammodel.sdm.performanceTest;

import java.util.ArrayList;

import umlsequencediagrammodel.sdm.Parser;
import umlsequencediagrammodel.sdm.UMLSequenceDiagram;

/**
 * 
 * @author Anusree Poduval 
 * @author Suishmitaa Srianand
 */
public class Sdm_V_1_2_PerformanceTest {
	  public static void main(String args[]) throws Exception  {
		  
		String filePath = "C:\\Users\\ROHITH\\Desktop\\demo.java";
	    long startTime = System.nanoTime();
	    
	    ArrayList<UMLSequenceDiagram> allSDM = Parser.parseFile(filePath);
	    long endTime = System.nanoTime();
	    System.out.println((endTime-startTime)/1000000000.0 + " seconds");
	    System.out.println(allSDM.get(0));
	    
	  }
	}
