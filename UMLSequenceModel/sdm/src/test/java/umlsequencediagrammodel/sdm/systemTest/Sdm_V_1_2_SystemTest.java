package umlsequencediagrammodel.sdm.systemTest;

import java.util.ArrayList;
import java.util.HashMap;

import umlsequencediagrammodel.sdm.Message;
import umlsequencediagrammodel.sdm.Parser;
import umlsequencediagrammodel.sdm.UMLMethod;
import umlsequencediagrammodel.sdm.UMLSequenceDiagram;
/**
 * 
 * @author Anusree Poduval
 * @author Suishmitaa Srianand
 *
 */
public class Sdm_V_1_2_SystemTest {
  public static void main(String args[]) throws Exception  {
	  HashMap<Integer, Message> map = new  HashMap<Integer, Message>();
	  Message message = new Message();
	  message.setFrom("c:Computer");
	  message.setTo("c:Computer");
	  message.setMessageName("display();");
	  map.put(1,message);
	  
	  UMLMethod method = new UMLMethod();
	  method.setName("main");
	  UMLSequenceDiagram sdm = new UMLSequenceDiagram(map,method);
    
String filePath = "C:\\Users\\ROHITH\\Desktop\\Tests\\test4.java";
    
    ArrayList<UMLSequenceDiagram> allSDM = Parser.parseFile(filePath);
    System.out.println(sdm.toString().equals((allSDM.get(0).toString())));
    
  }
}
