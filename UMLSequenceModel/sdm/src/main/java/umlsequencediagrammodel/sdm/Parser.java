package umlsequencediagrammodel.sdm;

import com.github.javaparser.*;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.resolution.UnsolvedSymbolException;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implements the javaparser to parse through the java source code and generates the UMLSequence
 * Diagram Model.
 * 
 * @author suishmitaa srianand
 * @author komal sharma
 * @author anusree poduval
 * @author ronit bhardwaj
 * @author piyush kochchar
 *
 */
public class Parser {

  /**
   * Main method.
   * 
   * @param args Commandline arguments
   * @throws Exception FileNotFoundException
   */
  public static void main(String[] args) throws Exception {
    String filePath =
        "C:\\Users\\suish\\OneDrive\\Desktop\\Semester 3\\Software Engineering\\demo - Copy.java";

    ArrayList<UMLSequenceDiagram> allSDM = parseFile(filePath);
    if (allSDM.isEmpty()) {
      System.out.println("No use cases identified.");
    } else {
      for (UMLSequenceDiagram sdm : allSDM)
        System.out
            .println("\nSequence Diagram for the use case " + sdm.getMethod().getName() + sdm);
    }
  }

  /**
   * Parses input java source code file and extracts class and method information.
   * 
   * @param filePath Input java source code file
   * @return Array list of UMLSequence Diagrams
   * @throws Exception
   */
  public static ArrayList<UMLSequenceDiagram> parseFile(String filePath) throws Exception {
    ArrayList<UMLClass> umlClassList = new ArrayList<UMLClass>(); // Array list of all the classes
                                                                  // in the input file.
    CompilationUnit cu = JavaParser.parse(new FileInputStream(filePath));
    List<TypeDeclaration<?>> types = cu.getTypes();
    for (TypeDeclaration<?> type : types) {
      List<BodyDeclaration<?>> bodyDeclarations = type.getMembers();
      UMLClass umlClass = new UMLClass();
      umlClass.setName(type.getNameAsString());
      umlClassList.add(umlClass);
      for (BodyDeclaration<?> body : bodyDeclarations) {
        if (body instanceof MethodDeclaration) {
          createUMLMethods(umlClass, (MethodDeclaration) body, false);
        } else if (body instanceof ConstructorDeclaration) {
          createUMLMethods(umlClass, (ConstructorDeclaration) body, true);
        }
      }
    }
    getTypeOfReference(filePath, umlClassList);
    return generate(umlClassList);
  }

  /**
   * Creates UMLMethod and constructors based on the information from the input file.
   * 
   * @param umlClass UMLClass
   * @param body method body block
   * @param isConstructor sets to true if the method block is a constructor.
   */
  private static void createUMLMethods(UMLClass umlClass, BodyDeclaration<?> body,
      boolean isConstructor) {
    UMLMethod umlMethod = new UMLMethod();

    // Constructor
    if (isConstructor) {
      ConstructorDeclaration constructor = (ConstructorDeclaration) body;
      umlMethod.setConstructor(true);
      umlMethod.setName(constructor.getNameAsString());
      umlMethod.setParameters(constructor.getParameters());
      umlMethod.setBlock(parseMethodBody(umlClass, constructor.getBody().toBlockStmt().get()));

    }
    // Method
    else {
      MethodDeclaration method = (MethodDeclaration) body;
      umlMethod.setConstructor(false);
      umlMethod.setName(method.getNameAsString());
      umlMethod.setParameters(method.getParameters());
      umlMethod.setType(method.getType());
      umlMethod.setBlock(parseMethodBody(umlClass, method.getBody().get()));
    }
    umlClass.getUMLMethods().add(umlMethod);
  }

  /**
   * 
   * Parses through the method body and extracts method calls alone.
   * 
   * @param umlClass UMLClass
   * @param methodBody method body declaration
   * @return Block of statement that contains only method calls
   */
  private static BlockStmt parseMethodBody(UMLClass umlClass, BlockStmt methodBody) {
    if (methodBody == null || methodBody.getStatements() == null) {
      return null;
    }
    BlockStmt block = new BlockStmt();
    List<MethodCallExpr> methodCalls = (methodBody.findAll(MethodCallExpr.class)); // Returns all
                                                                                   // method calls
                                                                                   // from the AST.

    for (MethodCallExpr mc : methodCalls) {
      if (!mc.getNameAsString().equals("println") && !mc.getNameAsString().equals("print")
          && !mc.getNameAsString().equals("printf")) // Ignores the print statement itself but picks
                                                     // up the method calls inside print statements.
        block.addStatement(mc);
    }
    return block;
  }

  /**
   * Uses symbol solver to solve the reference type of Method calls and stores them as a HashMap for
   * each UMLClass.
   * 
   * @param filePath Input file path
   * @param umlClassList List of all the classes in the input file
   */
  private static void getTypeOfReference(String filePath, ArrayList<UMLClass> umlClassList) {

    ArrayList<String> ref = new ArrayList<String>(); // HashMap value
    ArrayList<String> ref1 = new ArrayList<String>(); // HashMap key
    String previous; // Stores name of the class
    TypeSolver typeSolver = new ReflectionTypeSolver();
    JavaSymbolSolver symbolSolver = new JavaSymbolSolver(typeSolver);
    JavaParser.getStaticConfiguration().setSymbolResolver(symbolSolver);
    CompilationUnit cu;
    try {
      cu = JavaParser.parse(new File(filePath));
      List<MethodCallExpr> m = cu.findAll(MethodCallExpr.class);
      previous = m.get(0).findAncestor(ClassOrInterfaceDeclaration.class).get().getNameAsString();
      // Solving each method call
      for (MethodCallExpr mce : m) {
        if ((!mce.getNameAsString().equals("println") && !mce.getNameAsString().equals("print")
            && !mce.getNameAsString().equals("printf"))) {
          // To create a new hashmap for every class present
          if (!previous.equals(
              mce.findAncestor(ClassOrInterfaceDeclaration.class).get().getNameAsString())) {
            addMethodRefToClass(umlClassList, previous, ref1, ref);
            ref.clear();
            ref1.clear();
            previous = mce.findAncestor(ClassOrInterfaceDeclaration.class).get().getNameAsString();
          }
          if (!mce.toString().contains(".")) // static methods don't have a symbol to resolve
            ref.add(mce.findAncestor(ClassOrInterfaceDeclaration.class).get().getNameAsString());
          else {
            if (mce.resolve() != null)
              ref.add(mce.resolve().getClassName());
            else
              continue;
          }
        }
      }
      addMethodRefToClass(umlClassList, previous, ref1, ref);
    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    } catch (UnsolvedSymbolException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Adds the values from two arrays into the HashMap with each value as key-value pair.
   * 
   * @param umlClassList List of UMLCLass objects
   * @param previous Name of the class
   * @param ref1 HashMap Key
   * @param ref HashMap Value
   */
  private static void addMethodRefToClass(ArrayList<UMLClass> umlClassList, String previous,
      ArrayList<String> ref1, ArrayList<String> ref) {
    for (UMLClass umlClass : umlClassList) {
      if (umlClass.getName().equals(previous)) {
        for (int i = 0; i < umlClass.getUMLMethods().size(); i++) {
          for (Statement s : umlClass.getUMLMethods().get(i).getBlock().getStatements()) {
            if (s.toString().contains(".")) // static methodcalls
              ref1.add(s.toString());
            else
              ref1.add(s.toString().split("\\.")[0]);
          }
        }
        for (int i = 0; i < ref.size(); i++) {
          umlClass.getMethodCallReference().put(ref1.get(i), ref.get(i));
        }
      }
    }
  }

  /**
   * Puts all the information collected into the Sequence Diagram Model.
   * 
   * @param umlClassList List of UMLClasses
   * @return ArrayList of UMLSequence Diagram
   */
  private static ArrayList<UMLSequenceDiagram> generate(ArrayList<UMLClass> umlClassList) {
    ArrayList<UMLSequenceDiagram> allSDM = new ArrayList<UMLSequenceDiagram>();
    try {
      for (UMLClass umlClass : umlClassList) { // For each class
        for (UMLMethod m : umlClass.getUMLMethods()) { // Each method in a class corresponds to a
                                                       // sequence diagram
          if (m.getBlock().getStatements().isNonEmpty()) { // For each statement in the method block
            UMLSequenceDiagram sdm = new UMLSequenceDiagram(m); // Create a UMLSequenceDiagramObject
            int id = 1;
            for (Statement s : m.getBlock().getStatements()) { // For each methodcall statement
              Message message = new Message(); // Create a message
              String toFrom, me;
              if (!s.toString().contains(".")) { // For static methodcall, the message is a
                                                 // self-message.
                toFrom = umlClass.getName();
                me = s.toString();
              } else {
                toFrom = s.toString().split("\\.")[0];
                me = s.toString().split("\\.")[1];
              }
              if (umlClass.getName().equals(umlClass.getMethodCallReference().get(s.toString()))) { // Method
                                                                                                    // call
                                                                                                    // symbol
                                                                                                    // is
                                                                                                    // resolved
                message.setFrom(toFrom + ":" + umlClass.getName());
                message.setTo(toFrom + ":" + umlClass.getMethodCallReference().get(s.toString()));
                if (!s.toString().contains(".")) {
                  message.setFrom(umlClass.getName());
                  message.setTo(toFrom);
                }
              } else {
                message.setFrom(umlClass.getName());
                message.setTo(toFrom + ":" + umlClass.getMethodCallReference().get(s.toString()));
              }
              message.setMessageName(me);
              m.getMessageList().add(message);
              sdm.getSdm().put(id, message);
              id++;
            }
            allSDM.add(sdm);
          }
        }
      }
    } catch (ArrayIndexOutOfBoundsException e) {
      System.out.println(e.getMessage());
      e.printStackTrace();
    }
    return allSDM;
  }
}
