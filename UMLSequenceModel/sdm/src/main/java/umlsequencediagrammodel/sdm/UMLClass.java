package umlsequencediagrammodel.sdm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Models all information of a class present in the java source input file that's been extracted
 * using the javaparser.
 * 
 * @author suishmitaa srianand
 *
 */
public class UMLClass {

  private List<UMLMethod> umlMethods; // methods in the class
  private String name; // Name of the class
  private HashMap<String, String> methodCallReference; // HashMap with key-value as method call and
                                                       // invoking type.

  public UMLClass() {
    name = "";
    umlMethods = new ArrayList<>();
    methodCallReference = new HashMap<String, String>();
  }

  public HashMap<String, String> getMethodCallReference() {
    return methodCallReference;
  }

  public List<UMLMethod> getUMLMethods() {
    return umlMethods;
  }

  public void setUMLMethods(List<UMLMethod> umlMethods) {
    this.umlMethods = umlMethods;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
