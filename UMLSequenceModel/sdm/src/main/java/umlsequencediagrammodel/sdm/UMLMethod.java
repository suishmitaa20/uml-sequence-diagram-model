package umlsequencediagrammodel.sdm;

import java.util.ArrayList;
import java.util.List;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.type.Type;

/**
 * Models a method block statement.
 * 
 * @author suishmitaa srianand
 *
 */
public class UMLMethod {

  private String name; // Name of the method
  private List<Parameter> parameters; // input method parameters
  private boolean isConstructor; // indicates if the method block is a constructor or not
  private Type type; // return type of the method
  private BlockStmt block; // method block statement
  private ArrayList<Message> messageList; // Messages corresponding to each method call statement


  public UMLMethod() {
    parameters = new ArrayList<>();
    messageList = new ArrayList<Message>();
    block = new BlockStmt();
  }

  public ArrayList<Message> getMessageList() {
    return messageList;
  }

  public void setMessageList(ArrayList<Message> messageList) {
    this.messageList = messageList;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Parameter> getParameters() {
    return parameters;
  }

  public void setParameters(List<Parameter> parameters) {
    this.parameters = parameters;
  }

  public boolean isConstructor() {
    return isConstructor;
  }

  public void setConstructor(boolean isConstructor) {
    this.isConstructor = isConstructor;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public void setBlock(BlockStmt type) {
    this.block = type;
  }

  public BlockStmt getBlock() {
    return block;
  }
}
