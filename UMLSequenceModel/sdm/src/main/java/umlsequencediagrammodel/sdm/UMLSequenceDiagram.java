package umlsequencediagrammodel.sdm;

import java.util.HashMap;
import java.util.Map.Entry;

/**
 * Models the UML Sequence Diagram.
 * 
 * @author suishmitaa srianand
 *
 */
public class UMLSequenceDiagram {
  private HashMap<Integer, Message> sdm;
  private UMLMethod method;


  public UMLMethod getMethod() {
    return method;
  }

  public void setMethod(UMLMethod method) {
    this.method = method;
  }

  public UMLSequenceDiagram(UMLMethod method) {
    sdm = new HashMap<Integer, Message>();
    this.method = method;
  }

  public UMLSequenceDiagram(HashMap<Integer, Message> sdm, UMLMethod method) {
    this.sdm = sdm;
    this.method = method;
  }

  public HashMap<Integer, Message> getSdm() {
    return sdm;
  }

  public void setSdm(HashMap<Integer, Message> sdm) {
    this.sdm = sdm;
  }

  public String toString() {
    String s = "";
    for (Entry<Integer, Message> entry : sdm.entrySet()) {
      s += "\n" + entry.getValue().getFrom() + "-----" + entry.getKey() + "."
          + entry.getValue().getMessageName().replaceAll("\\;", "") + "----->"
          + entry.getValue().getTo();
    }
    return s;
  }
}
