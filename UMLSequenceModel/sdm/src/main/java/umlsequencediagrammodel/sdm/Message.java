package umlsequencediagrammodel.sdm;

/**
 * Models the message model in the Sequence Diagram.
 * 
 * @author suishmitaa srianand
 *
 */
public class Message {
  private String from;
  private String to;
  private String messageName;

  public Message(String from, String to, String messageName) {
    this.from = from;
    this.to = to;
    this.messageName = messageName;
  }

  public Message() {
    from = null;
    to = null;
    messageName = null;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public String getMessageName() {
    return messageName;
  }

  public void setMessageName(String nameOfInteraction) {
    this.messageName = nameOfInteraction;
  }

  public String toString() {
    return from + " ------ " + messageName.replaceAll("\\;", "") + "--------> " + to;
  }
}
