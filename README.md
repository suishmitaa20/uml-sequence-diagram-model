# UML Sequence Diagram Model (SDM) #

This repository implements the javaparser to parse through the input java source code file and generate the UML Sequence Diagram Model. This project also has a GUI built using JavaFX.

### Description of the branches in the repository ###

* master - contains the UML Sequence Diagram Model generator and all the IEEE standard documents written during the course of the project
* GUI_JavaFX - JavaFX code that has a GUI and implements the SDM that has been added as an external JAR to the project.

### How do I get set up? ###

Please refer to the User Manual present in the Documentation folder in the master branch to set up the project.

### Authors ###

* Suishmitaa Srianand
* Anusree Poduval
* Piyush Kochhar
* Komal Sharma
* Ronit Bhardwaj